# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [1.7.1](https://gitlab.com/walkchicken/practice-responsive-bootstrap/compare/v1.7.0...v1.7.1) (2021-12-17)


### Bug Fixes

* additional edit carousel projects and carousel team [#95](https://gitlab.com/walkchicken/practice-responsive-bootstrap/issues/95) ([8de1edb](https://gitlab.com/walkchicken/practice-responsive-bootstrap/commit/8de1edb42aefb5515fcb3115478e975b44154a64))
* delete unnecessary events [#96](https://gitlab.com/walkchicken/practice-responsive-bootstrap/issues/96) ([076d9f9](https://gitlab.com/walkchicken/practice-responsive-bootstrap/commit/076d9f9fbbab76aec1ab00ea50e8a3699ea4a6c9))
* edit UI/UX for subscribe form ([ac70472](https://gitlab.com/walkchicken/practice-responsive-bootstrap/commit/ac7047263e2f08b9caae332e6b26421c27bc6da6))

## [1.7.0](https://gitlab.com/walkchicken/practice-responsive-bootstrap/compare/v1.6.2...v1.7.0) (2021-12-16)


### Features

* add css for advantages section desktop screen ([f8444c4](https://gitlab.com/walkchicken/practice-responsive-bootstrap/commit/f8444c472ef74509838556f41955e21f58ca1a2a))
* add css for cta section desktop screen ([be61e19](https://gitlab.com/walkchicken/practice-responsive-bootstrap/commit/be61e1984ff73f0b6c5b0e4840b890f2f1273154))
* add css for header desktop screen ([9e830ee](https://gitlab.com/walkchicken/practice-responsive-bootstrap/commit/9e830ee369345ca2c44f853d2050f487051e5f5f))
* add css for pricing desktop screen ([c4a512a](https://gitlab.com/walkchicken/practice-responsive-bootstrap/commit/c4a512a5c583bb237d2ae0565b5f02a3d6fde740))
* add css for projects section tablet screen [#61](https://gitlab.com/walkchicken/practice-responsive-bootstrap/issues/61) ([8317915](https://gitlab.com/walkchicken/practice-responsive-bootstrap/commit/8317915dc59e403b606c29ae8c2ac2660cd664d4))
* add css for team section tablet screen [#62](https://gitlab.com/walkchicken/practice-responsive-bootstrap/issues/62) ([e13ab8b](https://gitlab.com/walkchicken/practice-responsive-bootstrap/commit/e13ab8b763a07ac227932324f72bb0c1d5fa56ef))
* add css for team section tablet screen [#62](https://gitlab.com/walkchicken/practice-responsive-bootstrap/issues/62) ([5b6ca14](https://gitlab.com/walkchicken/practice-responsive-bootstrap/commit/5b6ca149ca1c24736ee69334891d3fceafd4cb2d))
* add css for testimonials section desktop screen ([aec94d0](https://gitlab.com/walkchicken/practice-responsive-bootstrap/commit/aec94d0d5fb5a73f2ba2928f68f2ea546ae37702))
* add css to section pricing for tablet screen ([f4a0843](https://gitlab.com/walkchicken/practice-responsive-bootstrap/commit/f4a0843dfcec142e1c777a3137d4486d245a743e))
* add css to section pricing for tablet screen [#60](https://gitlab.com/walkchicken/practice-responsive-bootstrap/issues/60) ([d680107](https://gitlab.com/walkchicken/practice-responsive-bootstrap/commit/d680107d154977336b25a2fddd5bd368e86f1c8d))
* add css to the section testimonials for tablet screen [#59](https://gitlab.com/walkchicken/practice-responsive-bootstrap/issues/59) ([a9d9ac9](https://gitlab.com/walkchicken/practice-responsive-bootstrap/commit/a9d9ac9352eb568ae2abf6fd19d4478309251840))
* adding css to footer for mobile screen [#55](https://gitlab.com/walkchicken/practice-responsive-bootstrap/issues/55) ([15da2ce](https://gitlab.com/walkchicken/practice-responsive-bootstrap/commit/15da2ceeeb2ef076b46cc273dea708444decaaf0))
* adding css to header for tablet screen ([c9432dc](https://gitlab.com/walkchicken/practice-responsive-bootstrap/commit/c9432dcdf550cb1eebc4f300c111fba3ea981094))
* adding css to section advantages for tablet screen ([adb0c04](https://gitlab.com/walkchicken/practice-responsive-bootstrap/commit/adb0c048121038f8414cf6b1b87511c3543a0ad0))
* adding css to section advantages for tablet screen ([41896e0](https://gitlab.com/walkchicken/practice-responsive-bootstrap/commit/41896e04585a77b7949937f322aed3a58dbf346e))
* create branch feature/element_class_name_structure_and_adding_css_for_section_team_section_contact_and_footer ([eb8a16e](https://gitlab.com/walkchicken/practice-responsive-bootstrap/commit/eb8a16e58a27b4e893b17763987e73bf3ee6d272))


### Bug Fixes

* [BUG] edit hover button [#86](https://gitlab.com/walkchicken/practice-responsive-bootstrap/issues/86) ([9159dca](https://gitlab.com/walkchicken/practice-responsive-bootstrap/commit/9159dca180d8b93ea38c310d51d4739413e38fdb))
* [BUG] edit icon size [#88](https://gitlab.com/walkchicken/practice-responsive-bootstrap/issues/88) ([6b6c75e](https://gitlab.com/walkchicken/practice-responsive-bootstrap/commit/6b6c75edb42eab11e4da238077143c3b6363e74f))
* [BUG] edit outline button and input [#89](https://gitlab.com/walkchicken/practice-responsive-bootstrap/issues/89) ([66c56c5](https://gitlab.com/walkchicken/practice-responsive-bootstrap/commit/66c56c59f1e4a8717304feb022f3649bccb17cb7))
* [BUG] edit pagination [#87](https://gitlab.com/walkchicken/practice-responsive-bootstrap/issues/87) ([0b6b0a9](https://gitlab.com/walkchicken/practice-responsive-bootstrap/commit/0b6b0a9a98dab5745bcd0cc6f238bcdae2cdb79a))
* [BUG] edit text and border color ([76426b6](https://gitlab.com/walkchicken/practice-responsive-bootstrap/commit/76426b63e850068c6fbdf9779f082a17847991f4))
* [BUG] edit text overflow [#91](https://gitlab.com/walkchicken/practice-responsive-bootstrap/issues/91) ([f194941](https://gitlab.com/walkchicken/practice-responsive-bootstrap/commit/f194941a2b014946ec52741044f34199cf67b87b))
* [BUG] video editing and video button ([393d7c0](https://gitlab.com/walkchicken/practice-responsive-bootstrap/commit/393d7c07f161fcb6dbf82129181159127b530e12))
* add and custom style for footer tablet screen [#64](https://gitlab.com/walkchicken/practice-responsive-bootstrap/issues/64) ([3862819](https://gitlab.com/walkchicken/practice-responsive-bootstrap/commit/386281900c5fd645abaf7b79855ea8b6a7fc3290))
* add box-shawdow in pricing card ([bea3fba](https://gitlab.com/walkchicken/practice-responsive-bootstrap/commit/bea3fba9b9bd0f4fbec7641cbc895ff675c02b20))
* add favicon into the page ([a9c72e3](https://gitlab.com/walkchicken/practice-responsive-bootstrap/commit/a9c72e3cca9ccbf504a5c35cbd7f6a16f4b43850))
* apply address element [#92](https://gitlab.com/walkchicken/practice-responsive-bootstrap/issues/92) ([6e083c7](https://gitlab.com/walkchicken/practice-responsive-bootstrap/commit/6e083c7d98bdfb83c0e2b522f6e915b4066a0da7))
* check footer mobile screen ([d4ab8a5](https://gitlab.com/walkchicken/practice-responsive-bootstrap/commit/d4ab8a5aafd44c9d55cebfd8f66642b1aa3d5f80))
* edit element class name nav-link ([e0b499d](https://gitlab.com/walkchicken/practice-responsive-bootstrap/commit/e0b499df24102a747b7959e5a32a69b1ba3e2c6e))
* edit readme.md file ([f7ebe3e](https://gitlab.com/walkchicken/practice-responsive-bootstrap/commit/f7ebe3e5bfcb00087f58c572cd79e182aca6861d))
* element class name structure into the navigation for mobile screen ([a15b2e2](https://gitlab.com/walkchicken/practice-responsive-bootstrap/commit/a15b2e227f668a6df69967e0c2601c81d28cb2c0))
* element class name structure with BEM and add css for desktop screen ([1cd92c8](https://gitlab.com/walkchicken/practice-responsive-bootstrap/commit/1cd92c8d73bf1c9c78bc0fe4bfb31d160eb02de0))
* element class name structure with BEM and format css for mobile screen ([f6d78ba](https://gitlab.com/walkchicken/practice-responsive-bootstrap/commit/f6d78ba01868e54ce869bf7838059f9182aa7e87))
* element classname structure with tablet [#75](https://gitlab.com/walkchicken/practice-responsive-bootstrap/issues/75) ([8e56413](https://gitlab.com/walkchicken/practice-responsive-bootstrap/commit/8e5641332648e04cc7430ccd114bae561105c96f))
* element_class_name_structure for header banner and scroll down ([be0ed7b](https://gitlab.com/walkchicken/practice-responsive-bootstrap/commit/be0ed7b299018140268260aa09f110efc0885f4f))
* finish ([afaac3c](https://gitlab.com/walkchicken/practice-responsive-bootstrap/commit/afaac3c3e47e9411fef7b657812602a163cc7b37))
* format css file ([d3eb513](https://gitlab.com/walkchicken/practice-responsive-bootstrap/commit/d3eb513f4bd9703a1b9e65a16e6df048e9f9212e))
* overflow hidden section advantages for macbook screen ([0c2a468](https://gitlab.com/walkchicken/practice-responsive-bootstrap/commit/0c2a468cc0b00204c214d7073d276ba314479e81))

### [1.6.2](https://gitlab.com/walkchicken/practice-responsive-bootstrap/compare/v1.6.1...v1.6.2) (2021-11-30)

### [1.6.1](https://gitlab.com/walkchicken/practice-responsive-bootstrap/compare/v1.6.0...v1.6.1) (2021-11-29)

## [1.6.0](https://gitlab.com/walkchicken/practice-responsive-bootstrap/compare/v1.5.0...v1.6.0) (2021-11-26)


### Features

* add section hero cta and section testimonias ([8b8f046](https://gitlab.com/walkchicken/practice-responsive-bootstrap/commit/8b8f046d0590250256cb7a3cae3608ff725a66af)), closes [#19](https://gitlab.com/walkchicken/practice-responsive-bootstrap/issues/19) [#20](https://gitlab.com/walkchicken/practice-responsive-bootstrap/issues/20) [#21](https://gitlab.com/walkchicken/practice-responsive-bootstrap/issues/21) [#22](https://gitlab.com/walkchicken/practice-responsive-bootstrap/issues/22) [#23](https://gitlab.com/walkchicken/practice-responsive-bootstrap/issues/23) [#24](https://gitlab.com/walkchicken/practice-responsive-bootstrap/issues/24) [#25](https://gitlab.com/walkchicken/practice-responsive-bootstrap/issues/25)
* creating 4 section, footer for mobile screen ([c02d706](https://gitlab.com/walkchicken/practice-responsive-bootstrap/commit/c02d70645d1a643948f5f8eb5941261d30d77488)), closes [#26](https://gitlab.com/walkchicken/practice-responsive-bootstrap/issues/26) [#29](https://gitlab.com/walkchicken/practice-responsive-bootstrap/issues/29) [#34](https://gitlab.com/walkchicken/practice-responsive-bootstrap/issues/34) [#37](https://gitlab.com/walkchicken/practice-responsive-bootstrap/issues/37) [#42](https://gitlab.com/walkchicken/practice-responsive-bootstrap/issues/42)

## [1.5.0](https://gitlab.com/walkchicken/practice-responsive-bootstrap/compare/v1.4.2...v1.5.0) (2021-11-25)


### Features

* add section hero cta and section testimonias ([3313380](https://gitlab.com/walkchicken/practice-responsive-bootstrap/commit/3313380a10f612df3f4a6228ee3b1d292ba058fb)), closes [#19](https://gitlab.com/walkchicken/practice-responsive-bootstrap/issues/19) [#20](https://gitlab.com/walkchicken/practice-responsive-bootstrap/issues/20) [#21](https://gitlab.com/walkchicken/practice-responsive-bootstrap/issues/21) [#22](https://gitlab.com/walkchicken/practice-responsive-bootstrap/issues/22) [#23](https://gitlab.com/walkchicken/practice-responsive-bootstrap/issues/23) [#24](https://gitlab.com/walkchicken/practice-responsive-bootstrap/issues/24) [#25](https://gitlab.com/walkchicken/practice-responsive-bootstrap/issues/25)

### [1.4.2](https://gitlab.com/walkchicken/practice-responsive-bootstrap/compare/v1.4.1...v1.4.2) (2021-11-25)

### [1.4.1](https://gitlab.com/walkchicken/practice-responsive-bootstrap/compare/v1.4.0...v1.4.1) (2021-11-24)

## [1.4.0](https://gitlab.com/walkchicken/practice-responsive-bootstrap/compare/v1.3.1...v1.4.0) (2021-11-23)


### Features

* add icons menu navbar ([c7e6efd](https://gitlab.com/walkchicken/practice-responsive-bootstrap/commit/c7e6efd945ebefff04c13b9fc600b6084430e72f))

### [1.3.1](https://gitlab.com/walkchicken/practice-responsive-bootstrap/compare/v1.3.0...v1.3.1) (2021-11-23)

## [1.3.0](https://gitlab.com/walkchicken/practice-responsive-bootstrap/compare/v1.2.0...v1.3.0) (2021-11-22)


### Features

* Coding header : background [#7](https://gitlab.com/walkchicken/practice-responsive-bootstrap/issues/7) ([be23703](https://gitlab.com/walkchicken/practice-responsive-bootstrap/commit/be23703f4990c2a18e0ea8d4acfcae44ec721fca))

## [1.2.0](https://gitlab.com/walkchicken/practice-responsive-bootstrap/compare/v1.1.1...v1.2.0) (2021-11-22)


### Features

* add images on css folder [#6](https://gitlab.com/walkchicken/practice-responsive-bootstrap/issues/6) ([649e512](https://gitlab.com/walkchicken/practice-responsive-bootstrap/commit/649e512fe2c7ecad8fba172138c49a59cd99e855))

### [1.1.1](https://gitlab.com/walkchicken/practice-responsive-bootstrap/compare/v1.1.0...v1.1.1) (2021-11-22)


### Bug Fixes

* add folder css ([d31ad96](https://gitlab.com/walkchicken/practice-responsive-bootstrap/commit/d31ad96baf628810af010484e0454ffc4dd31525))

## 1.1.0 (2021-11-22)


### Features

* add standard release support! ([852c1dc](https://gitlab.com/walkchicken/practice-responsive-bootstrap/commit/852c1dc4e3a9e2c5c828fd9e7a36e0f21c82c707))


### Bug Fixes

* create file index.html [#5](https://gitlab.com/walkchicken/practice-responsive-bootstrap/issues/5) ([95ee02f](https://gitlab.com/walkchicken/practice-responsive-bootstrap/commit/95ee02f5970cfa73dad54a5b998cdfc4597e4da6))
