# Practice Responsive Bootstrap

Hofmann UI Kit - Page Templates - Desktop #1 - Tablet #1 - Phone #1
https://www.figma.com/file/3NPM1Zv0H1Vw16rEmYdoJS/Hofmann-UI-Kit---Page-Templates-(Copy)?node-id=0%3A2

## How to dowload and settings

clone repo - projects: Practice Responsive Bootstrap
cd Practice Responsive Bootstrap
npm i yarn

### Open app

cd Practice Responsive Bootstrap
yarn index.html
or open folder -> open file index.html

#### Demo app
https://nhatduong-responsivebootstrap.herokuapp.com/
